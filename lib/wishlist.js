const util = require('util');

module.exports = function (restClient) {
    var module = {};

    module.add = function (customerToken, sku) {
      return restClient.post('/wishlist/add', {sku: sku}, customerToken);
    };

    module.pull = function(customerToken) {
        return restClient.get('/wishlist', customerToken);
    };

    module.update = function (customerToken, wishItem) {
        return restClient.put('/wishlist/update', wishItem, customerToken)
    }

    module.addToCart = function (customerToken, wishItem) {
        return restClient.post('/wishlist/move-to-cart', wishItem, customerToken);
    }

    module.remove = function (customerToken, wishItemId) {
      var endpointUrl = util.format('/wishlist/remove/%d', wishItemId);
      return restClient.delete(endpointUrl, customerToken);
    }

    return module;
};
