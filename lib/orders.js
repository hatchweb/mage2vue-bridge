
module.exports = function (restClient) {
    var module = {};

    /**
     *
     * @see https://devdocs.magento.com/guides/v2.3/rest/retrieve-filtered-responses.html
     * @see salesOrderRepositoryV1: GET /V1/orders/{id}
     *
     * @param oderId
     * @returns {Promise<{increment_id: String}>}
     */
    module.incrementIdById = function (oderId) {
        return restClient.get('/orders/' + oderId + '?fields=increment_id');
    }

    module.addComment = function(orderId, data){
        return restClient.post('/orders/' + orderId + '/comments', data)
    }

    module.getDetail = function(orderId){
        return restClient.get('/orders/' + orderId)
    }
    
    return module;
}
